print("PERIODIC TABLE")
print()
print("What is a Periodic Table?")
print("A table of the chemical elements arranged in order of atomic number, usually in rows, so that elements with similar atomic structure (and hence similar chemical properties) appear in vertical columns.")
print()
print("Brief history of Periodic Table: ")
print("In 1869 Russian chemist Dimitri Mendeleev started the development of the periodic table, arranging chemical elements by atomic mass. He predicted the discovery of other elements, and left spaces open in his periodic table for them.")
print()

elements_20 = ["Hydrogen", "Helium", "Lithium", "Berryllium", "Boron", "Carbon", "Nitrogen", "Oxygen", "Flourine", "Neon", "Sodium", "Magnesium", "Alluminium", "Silicon", "Phosphorus", "Sulphur", "Chlorine", "Argon", "Pottasium", "Calcium"]

print("First 20 elements in Periodic Table")
print()
for i in range (len(elements_20)):
  print(elements_20[i])